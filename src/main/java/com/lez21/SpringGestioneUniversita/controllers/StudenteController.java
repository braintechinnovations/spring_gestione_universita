package com.lez21.SpringGestioneUniversita.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.lez21.SpringGestioneUniversita.models.Studente;
import com.lez21.SpringGestioneUniversita.models.StudenteResponse;
import com.lez21.SpringGestioneUniversita.services.StudenteService;

@RestController
@RequestMapping("/studente")
public class StudenteController {

	@Autowired
	private StudenteService serviceStud;
	
	@PostMapping("/nuovo")
	public StudenteResponse inserisciStudente(@RequestBody Studente studObj) {
		
		StudenteResponse response = new StudenteResponse();
		Studente temp = this.serviceStud.insert(studObj);
		
		if(temp != null) {
			response.setStatus("SUCCESS");
			response.getElenco().add(temp);
		}
		else {
			response.setStatus("ERROR");
		}
		
		return response;
	}
	
	@GetMapping("/{studente_id}")
	public StudenteResponse restituisciStudente(@PathVariable(name="studente_id") int varId) {
		
		StudenteResponse response = new StudenteResponse();	//Creo un oggetto uniforme chiamato Response
		Studente temp = this.serviceStud.findById(varId);

		if(temp != null) {									//Nel caso in cui l'oggetto Studente è stato trovato...
			response.setStatus("SUCCESS");
			response.getElenco().add(temp);
		}
		else {
			response.setStatus("NOT_FOUND");
		}
		
		return response;
	}
	
	@GetMapping("/lista")
	public StudenteResponse elencoStudenti(){
		StudenteResponse response = new StudenteResponse();
		
		List<Studente> risultati = this.serviceStud.findAll();
		
		if(risultati.size() != 0) {
			response.setStatus("SUCCESS");
			response.setElenco(risultati);
		}
		else {
			response.setStatus("EMPTY_SET");
		}
		
		return response;
	}
	
	//TODO: Update
	
	//TODO: Delete
	
}
