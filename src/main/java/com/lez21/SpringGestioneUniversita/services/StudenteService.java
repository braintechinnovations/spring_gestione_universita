package com.lez21.SpringGestioneUniversita.services;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.transaction.Transactional;

import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.lez21.SpringGestioneUniversita.models.Studente;
import com.lez21.SpringGestioneUniversita.repositories.DataAccessRepo;

@Service
public class StudenteService implements DataAccessRepo<Studente> {

	@Autowired
	private EntityManager ent_man;
	
	private Session getSessione() {
		return ent_man.unwrap(Session.class);
	}
	
	@Override
	public Studente insert(Studente t) {
		
		Studente temp = new Studente();
		temp.setMatricola(t.getMatricola());
		temp.setNome(t.getNome());
		temp.setCognome(t.getCognome());
		
		Session sessione = getSessione();
		
		try {
			sessione.save(temp);
			return temp;
		} catch (Exception e) {
			System.out.println(e.getMessage());
			return null;
		} finally {
			sessione.close();
		}
		
	}

	@Override
	public Studente findById(int id) {
		Session sessione = getSessione();

        try {
        	//Costruisco il Builder dei criteri di ricerca tramite il quale posso generare le query
            CriteriaBuilder cBuilder = ent_man.getCriteriaBuilder();
            //Creo una query di ricerca che si applica su tutti i campi della classe Studente (ATTRIBUTI)
            CriteriaQuery<Studente> cQuery = cBuilder.createQuery(Studente.class);

            //Equivalente di FROM Studente
            Root<Studente> rt = cQuery.from(Studente.class);

            //Equivalente di SELECT * ... WHERE Studente.id = id;
            cQuery.select(rt).where(cBuilder.equal(rt.get("id"), id));

            //Equivalente di un Prepared Statement
            Query query = sessione.createQuery(cQuery);

            //Restituisco un singolo risultato dalla query
            return (Studente) query.getSingleResult();

        } catch (Exception e) {
            return null;
        }
	}

	@Override
	public List<Studente> findAll() {
		Session sessione = getSessione();
		
		CriteriaQuery<Studente> cq = sessione.getCriteriaBuilder().createQuery(Studente.class);
		//Equivalente solo di FROM Studente
        cq.from(Studente.class);

        return sessione.createQuery(cq).getResultList();
	}

	@Override
	public boolean delete(int id) {
		// TODO Auto-generated method stub
		return false;
	}

	@Transactional
	@Override
	public boolean update(Studente t) {

		Session sessione = getSessione();
		
		try {
			sessione.update(t);
			return true;
		} catch (Exception e) {
			System.out.println(e.getMessage());
			return false;
		} finally {
			sessione.close();
		}
		
	}

}
