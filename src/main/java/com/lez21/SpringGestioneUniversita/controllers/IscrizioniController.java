package com.lez21.SpringGestioneUniversita.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.lez21.SpringGestioneUniversita.models.Esame;
import com.lez21.SpringGestioneUniversita.models.Studente;
import com.lez21.SpringGestioneUniversita.services.EsameService;
import com.lez21.SpringGestioneUniversita.services.StudenteService;

@RestController
@RequestMapping("/iscrizioni")
public class IscrizioniController {

	@Autowired
	private StudenteService serviceStud;
	@Autowired
	private EsameService serviceEsam;
	
	@GetMapping("/iscrivi/{studente_id}/{esame_id}")
	public String iscriviStudenteEsame(
			@PathVariable(name="studente_id") int studId,
			@PathVariable(name="esame_id") int esamId) {
		
		Studente tempStud = this.serviceStud.findById(studId);
		Esame tempEsam = this.serviceEsam.findById(esamId);
		
		if(tempStud != null && tempEsam != null) {
			tempStud.getElencoEsami().add(tempEsam);
			
			if(this.serviceStud.update(tempStud)) {
				return "OK";
			}
		}
		
		return "errore";
		
	}
	
	//TODO: Eliminare iscrizione all'esame?
}
