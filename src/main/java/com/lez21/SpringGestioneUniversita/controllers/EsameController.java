package com.lez21.SpringGestioneUniversita.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.lez21.SpringGestioneUniversita.models.Esame;
import com.lez21.SpringGestioneUniversita.models.EsameResponse;
import com.lez21.SpringGestioneUniversita.services.EsameService;

@RestController
@RequestMapping("/esame")
public class EsameController {

	@Autowired
	private EsameService serviceEsam;
	
	@PostMapping("/nuovo")
	public EsameResponse inserisciStudente(@RequestBody Esame esamObj) {
		
		EsameResponse response = new EsameResponse();
		Esame temp = this.serviceEsam.insert(esamObj);
		
		if(temp != null) {
			response.setStatus("SUCCESS");
			response.getElenco().add(temp);
		}
		else {
			response.setStatus("ERROR");
		}
		
		return response;
	}
	
	@GetMapping("/{esame_id}")
	public EsameResponse restituisciStudente(@PathVariable(name="esame_id") int varId) {
		
		EsameResponse response = new EsameResponse();	//Creo un oggetto uniforme chiamato Response
		Esame temp = this.serviceEsam.findById(varId);

		if(temp != null) {									//Nel caso in cui l'oggetto Studente è stato trovato...
			response.setStatus("SUCCESS");
			response.getElenco().add(temp);
		}
		else {
			response.setStatus("NOT_FOUND");
		}
		
		return response;
	}
	
	@GetMapping("/lista")
	public EsameResponse elencoStudenti(){
		EsameResponse response = new EsameResponse();
		
		List<Esame> risultati = this.serviceEsam.findAll();
		
		if(risultati.size() != 0) {
			response.setStatus("SUCCESS");
			response.setElenco(risultati);
		}
		else {
			response.setStatus("EMPTY_SET");
		}
		
		return response;
	}
	
}
