package com.lez21.SpringGestioneUniversita.services;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.lez21.SpringGestioneUniversita.models.Esame;
import com.lez21.SpringGestioneUniversita.repositories.DataAccessRepo;

@Service
public class EsameService implements DataAccessRepo<Esame>{

	@Autowired
	private EntityManager ent_man;
	
	private Session getSessione() {
		return ent_man.unwrap(Session.class);
	}
	
	@Override
	public Esame insert(Esame t) {
		
		Esame temp = new Esame();
		temp.setNome(t.getNome());
		temp.setCrediti(t.getCrediti());
		temp.setData_esame(t.getData_esame());
		
		Session sessione = getSessione();
		
		try {
			sessione.save(temp);
			return temp;
		} catch (Exception e) {
			System.out.println(e.getMessage());
			return null;
		} finally {
			sessione.close();
		}
		
	}

	@Override
	public Esame findById(int id) {
		Session sessione = getSessione();

        try {
        	//Costruisco il Builder dei criteri di ricerca tramite il quale posso generare le query
            CriteriaBuilder cBuilder = ent_man.getCriteriaBuilder();
            //Creo una query di ricerca che si applica su tutti i campi della classe Studente (ATTRIBUTI)
            CriteriaQuery<Esame> cQuery = cBuilder.createQuery(Esame.class);

            //Equivalente di FROM Studente
            Root<Esame> rt = cQuery.from(Esame.class);

            //Equivalente di SELECT * ... WHERE Studente.id = id;
            cQuery.select(rt).where(cBuilder.equal(rt.get("id"), id));

            //Equivalente di un Prepared Statement
            Query query = sessione.createQuery(cQuery);

            //Restituisco un singolo risultato dalla query
            return (Esame) query.getSingleResult();

        } catch (Exception e) {
            return null;
        }
	}

	@Override
	public List<Esame> findAll() {
		Session sessione = getSessione();
		
		CriteriaQuery<Esame> cq = sessione.getCriteriaBuilder().createQuery(Esame.class);
		//Equivalente solo di FROM Studente
        cq.from(Esame.class);

        return sessione.createQuery(cq).getResultList();
	}

	@Override
	public boolean delete(int id) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean update(Esame t) {
		// TODO Auto-generated method stub
		return false;
	}

}
