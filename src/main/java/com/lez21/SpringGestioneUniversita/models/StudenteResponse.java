package com.lez21.SpringGestioneUniversita.models;

import java.util.ArrayList;
import java.util.List;

/**
 * Questo uniforma le risposte, lo status può contenere una
 * stringa contenente "SUCCESS" oppure "ERROR"
 * @author SVILUPPO
 *
 */
public class StudenteResponse {

	private String status;
	private List<Studente> elenco = new ArrayList<Studente>();
	
	public StudenteResponse() {
		
	}
	
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public List<Studente> getElenco() {
		return elenco;
	}
	public void setElenco(List<Studente> elenco) {
		this.elenco = elenco;
	}
	
	
	
}
